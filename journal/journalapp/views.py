from rest_framework.decorators import permission_classes, authentication_classes
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from datetime import datetime
from django.db.models import Q
from .models import *

@api_view(['GET'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def attendance(request, format=None):
    if "group" not in request.GET:
        return Response({'errors': 'Group should be specified'})
    try:
        group = Group.objects.get(name=request.GET["group"])
    except:
        return Response({"error": "No such group " + request.GET["group"]})

    klasses = Klass.objects.filter(
        lesson__course__group__name=request.GET["group"]).order_by(
        'date', 'lesson__start_time')
    attendances = Attendance.objects.filter(klass__in=klasses)
    json = {"lessons": [k.jsonify() for k in klasses], "students": []}
    for student in group.user_set.all():
        json["students"].append({
            "name": student.full_name,
            "attendencies": [Attendance.objects.filter(klass=k).exists() for k in klasses]
        })

    return Response(json)

@api_view(['GET'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def group_list(request, format=None):
    json = {"groups": [str(g) for g in Group.objects.filter(~Q(name="Tutors"))]}
    return Response(json)

@api_view(['GET'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def course_list(request, format=None):
    json = {"courses": [{"id": c.id, "name": str(c)} for c in Course.objects.all()]}
    return Response(json)

@api_view(['GET'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def tutor_list(request, format=None):
    json = {"tutors": [{"id": t.id, "name": t.full_name} for t in User.objects.filter(groups__name="Tutors")]}
    return Response(json)

@api_view(['POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def create_course(request):
    print(request.data['group'])
    course = Course()
    course.name = request.data['name']
    course.start_date = datetime.strptime(request.data['start_date'], '%b %d %Y %I:%M%p')
    course.end_date = datetime.strptime(request.data['end_date'], '%b %d %Y %I:%M%p')
    course.save()
    course.group.add(*Group.objects.filter(name__in=request.data['group']))
    return Response({}, status=status.HTTP_201_CREATED)

@api_view(['POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def create_group(request, format=None):
    group = Group()
    try:
        group.name = request.data['name']
    except KeyError:
        return Response({"error": "'name' field is required"},
                        status=status.HTTP_400_BAD_REQUEST)
    group.save()
    return Response(status=status.HTTP_201_CREATED)

@api_view(['POST'])
@permission_classes((AllowAny, ))
def register(request, format=None):
    user = User()
    try:
        user.username = request.data['username']
        user.email = request.data['email']
        if request.data['password'] != request.data['password2']:
            return Response({"error": "Passwords does not match"},
                                   status=status.HTTP_400_BAD_REQUEST)
        user.set_password(request.data['password'])
        user.first_name = request.data['first_name']
        user.last_name = request.data['last_name']
    except KeyError as e:
        return Response({"error": "{0} field is required".format(e.args[0])},
                        status=status.HTTP_400_BAD_REQUEST)
    try:
        user.save()
    except ValidationError as e:
        return Response({"error": e.message},
                        status=status.HTTP_400_BAD_REQUEST)

    return Response(status=status.HTTP_201_CREATED)

@api_view(['GET'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def logout(request, format=None):
    request.user.auth_token.delete()
    return Response(status=status.HTTP_200_OK)

@api_view(['POST'])
@authentication_classes((TokenAuthentication, ))
@permission_classes((IsAuthenticated, ))
def mark_attendancies(request, format=None):
    print(request.data['klass'])
    if not request.user.groups.filter(name="Tutors"):
        return Response({"error": "Only tutor can mark attendances"},
                         status=status.HTTP_403_FORBIDDEN)
    try:
        klass = Klass.objects.get(pk=request.data["klass"])
    except:
        return Response({"error": "Class not found"},
                        status=status.HTTP_400_BAD_REQUEST)
    for id in request.data["students"]:
        try:
            student = User.objects.get(pk=id)
        except:
            continue
        attendance = Attendance()
        attendance.klass = klass
        attendance.student = student
        try:
            attendance.save()
        except:
            continue

    return Response(status=status.HTTP_201_CREATED)
