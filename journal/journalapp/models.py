from datetime import time, date, datetime
from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.utils.translation import ugettext
from django.contrib.auth.models import User, Group
import pytz

utc=pytz.UTC
User.full_name = property(lambda u: u"%s %s" % (u.first_name, u.last_name))

class Course(models.Model):
    name = models.CharField(max_length=128, null=False)
    group = models.ManyToManyField(Group, blank=False)
    start_date = models.DateField(null=False, default=date.today)
    end_date = models.DateField(null=False, default=date.today)

    def __str__(self):
        return "{0}: {1}".format(self.name, [g.name for g in self.group.all()])
    __repr__ = __str__

    def clean(self):
        if self.start_date > self.end_date:
            raise ValidationError(_("Course plan duration can't be negative."))

class Lesson(models.Model):
    start_time = models.TimeField(null=False, default=time(8, 00))
    end_time = models.TimeField(null=False, default=time(9, 20))
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    tutor = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    auditory = models.CharField(max_length=32)
    period = models.PositiveIntegerField(default=1)

    def __str__(self):
        return "{0}: {1} -- {2}".format(self.course.name,
                                        self.start_time,
                                        self.end_time)
    __repr__ = __str__

    def clean(self):
        if self.start_date is None:
            self.start_date = self.course.start_date
        if self.end_date is None:
            self.end_date = self.course.end_date

        if self.start_date > self.end_date:
            raise ValidationError(_("Lesson plan duration can't be negative."))
        if self.start_time > self.end_time:
            raise ValidationError(_("Lesson duration can't be negative."))

        if not self.tutor.groups.filter(name="Tutors").exists():
            raise ValidationError(_("{0} is not tutor".format(self.tutor.full_name)))

class Klass(models.Model):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    date = models.DateField(null=False, default=date.today)

    def jsonify(self):
        return {
            "id": self.id,
            "name": self.lesson.course.name,
            "date": str(self.date),
            "starts": str(self.lesson.start_time),
            "ends": str(self.lesson.end_time),
            "tutor": str(self.lesson.tutor.full_name),
        }

    def clean(self):
        if not (self.lesson.start_date <= self.date <= self.lesson.end_date):
            raise ValidationError(_("Invalid date"))
        if (self.date - self.lesson.start_date).days % (self.lesson.period * 7):
            raise ValidationError(_("Invalid date"))

    def __str__(self):
        return "{0}: {1}".format(self.date, str(self.lesson))
    __repr__ = __str__


class Attendance(models.Model):
    student = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    klass = models.ForeignKey(Klass, null=False, on_delete=models.CASCADE)

    def clean(self):
        if not set(self.student.groups.all()) & set(self.klass.lesson.course.group.all()):
            raise ValidationError(_("Student {0} can't attend course {1}".format(
                self.student.full_name, self.klass.lesson.course.name)))
        if not (utc.localize(datetime(
                self.klass.date.year, self.klass.date.month, self.klass.date.day,
                self.klass.lesson.start_time.hour,
                self.klass.lesson.start_time.minute,
                self.klass.lesson.start_time.second
            )) <= timezone.now() <= utc.localize(datetime(
                self.klass.date.year, self.klass.date.month, self.klass.date.day,
                self.klass.lesson.end_time.hour,
                self.klass.lesson.end_time.minute,
                self.klass.lesson.end_time.second
            ))):
            raise ValidationError(_("Marks can't be changed now"))

    class Meta:
        unique_together = (("student", "klass"),)

    def __str__(self):
        return self.student.full_name + ": " + str(self.klass)
    __repr__ = __str__
