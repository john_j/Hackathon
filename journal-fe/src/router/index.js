import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '@/components/LoginPage'
import UsrPage from '@/components/UsrPage'
import Chart from '@/components/Chart'
import AttendingGrid from '@/components/AttendingGrid'
import RatingGrid from '@/components/RatingGrid'
import Schedule from '@/components/Schedule'
import SignUpPage from '@/components/SignUpPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/', 
      name: 'LoginPage',
      component: LoginPage
    },
    {
    	path: '/UsrPage',
    	name: 'UsrPage',
    	component: UsrPage
    }, 
    {
    	path: '/Chart',
    	name: 'Chart',
    	component: Chart
    },
    {
    	path: '/AttendingGrid',
    	name: 'AttendingGrid',
    	component: AttendingGrid
    },
    {
    	path: '/RatingGrid',
    	name: 'RatingGrid',
    	component: RatingGrid
    },
    {
    	path: '/Schedule',
    	name: 'Schedule',
    	component: Schedule
    },
    {
    	path: '/SignUpPage',
    	name: 'SignUpPage',
    	component: SignUpPage
    }
  ]
})
